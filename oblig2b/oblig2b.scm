;;Oppgave 1a
(define (make-counter)
  (define x 0)
  (lambda () (set! x (+ 1 x))
    x))


;;Oppgave 2a
(define (make-stack stack)
  (lambda op
    (cond ((eq? (car op) 'pop!)
           (if (null? stack)
               (set! stack '())
               (set! stack (cdr stack))))
          ((eq? (car op) 'push!)
           (set! stack (append
                        (reverse (cdr op))
                        stack)))
          ((eq? (car op) 'stack) stack)
          (else (display "Unknown command: ")
                (display op)
                (newline)))))

;;Oppgave 2b
(define (pop! stack)
  (stack 'pop!))

(define (push! stack . arg-list)
  (define (inner args)
    (if (not (null? args))
        (begin (stack 'push! (car args))
               (inner (cdr args)))))
  (inner arg-list))

(define (stack stack)
  (stack 'stack))


(define s1 (make-stack (list 'foo 'bar)))
(define s2 (make-stack '()))
(s1 'pop!)
;;(s1 'stack) ;; (bar)
(s2 'pop!)
(s2 'push! 1 2 3 4)
;;(s2 'stack) ;; (4 3 2 1)
(s1 'push! 'bah)
(s1 'push! 'zap 'zip 'baz)
;;(s1 'stack) ;; (baz zip zap bah bar)
(pop! s1)
;;(stack s1) ;; (zip zap bah bar)
(push! s1 'foo 'faa)
;;(stack s1) ;; (faa foo zip zap bah bar)


;;Oppgave 3c
;;Predikatet list? sjekker om argumentet er ekte liste,
;;dvl. har endelig lengde og er terminert med en tom liste.

;;Sirkulære lister terminerer aldri, derfor regnes de som ikke-ekte-lister
;;og derfor returnerer list? false verdi når den sjekker for bar

;;bah derimot har legnde på 3 og avslutter med '(),
;;derfor får vi true når vi sjekker den med list?


(load "prekode2b.scm")

(define (true? op)
  (eq? op #t))

(define (false? op)
  (eq? op #f))


(define doc (make-table))
(insert! 'b 2 doc)

(define (memoized proc)
  (define table (make-table))
  (lambda (args)
    (if (false? (lookup args table))
        (insert! args (proc args) table))
    (lookup args table)))

(define (mem cmd proc)
  (cond ((eq? cmd 'memoize) (memoized proc))
        ((eq? cmd 'unmemoize) #f)
        (else (display "Unknown command: ")
              (display cmd)
              (newline))))
  
(define mem-fib (mem 'memoize fib))
(mem-fib 3)
(mem-fib 3)
(mem-fib 2)

(mem 'unmemosadasdize fib)


;;Oppgave 4c
;;For å beregne (mem-fib 3) må vi regne (fib 3), (fib 2) .. (fib 0)
;;Men disse beregningene blir ikke lagret i tabellen
