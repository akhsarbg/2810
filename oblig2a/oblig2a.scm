;;akhsarbg, kraraz, simontb

(load "huffman.scm")


;;a)
(define (p-cons x y)
  (lambda (proc) (proc x y)))


(define (p-car proc)
  (proc (lambda (x y) x)))

(define (p-cdr proc)
  (proc (lambda (x y) y)))



;;b)
(define foo 42)

#|
(let ((foo 5)
      (x foo))
  (if (= x foo)
      'same
      'different))
;;-> different
|#

((lambda (foo x)
   (if (= x foo)
       'same
       'different))
 5 foo)
;;-> different

#|
(let ((bar foo)
      (baz 'towel))
  (let ((bar (list bar baz))
	(foo baz))
    (list foo bar)))
;-> (towel (42 towel))
|#

((lambda (bar baz)
   ((lambda (bar foo)
      (list foo bar))
    (list bar baz) baz))
 foo 'towel)
;;-> (towel (42 towel))

;;c)
(define (infix-eval exp)
  ((cadr exp) (car exp) (caddr exp)))

;;d) vi får feilmelding fordi '(84 / 2) returnerer string value
;; og / regnes da ikke som en prosedyre.






;;Oppgave 2

;;a)
(define (member? eq element list)
  (cond ((null? list) #f)
	((eq element (car list)) #t)
	(else (member? eq element (cdr list)))))

#|b)
Det blir brukt en intern prosedyre fordi da tar vi i bruk current-branch til å kjøre
gjennom hele "grenen" og når vi da når bladet/enden så kan vi bruke det innsendte argumentet
tree til å gå til neste "gren", og derfor være sikre på at vi går gjennom hele treet.
|#

;;c)
(define (decode-halv bits tree)
  (define (decode-1 bits current-branch init)
    (if (null? bits)
	'()
	(let ((next-branch
	       (choose-branch (car bits) current-branch)))
	  (if (leaf? next-branch)
	      (decode-1 (cdr bits) tree (cons (symbol-leaf next-branch) init))
	      (decode-1 (cdr bits) next-branch init)))))
  (decode-1 bits tree '()))
