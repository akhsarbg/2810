;;akhsarbg, simontb og kraraz!
(load "prekode3a.scm")
;; Oppgave1a)
(define (list-to-stream list)
  (define (iter y)
    (if (null? y)
        '()
        (cons-stream (car y) (iter (cdr y)))))
  (iter list))

(define one (list-to-stream '(1)))
(define null (list-to-stream '()))
(define zeros (list-to-stream '(0 0 0)))
(define one-two (list-to-stream '(1 2)))
(define q (list-to-stream '(1 2 3 4)))
(define z (list-to-stream '(1 2 3 4 1 2 3)))

(define (stream-to-list stream . index)
  (define (iter s teller)
    (if (equal? index '()) ;; Her får vi en endelig strøm, og leser den. 
        (if (null? s)
            '()
            (cons (stream-car s) (iter (stream-cdr s) 0)))
        (cond ((null? s) '()) ;; dette er hvis det er en uendelig strøm
              ((equal? teller (car index)) '())
              (else (cons (stream-car s) (iter (stream-cdr s) (+ teller 1)))))))
  (iter stream 0))

;; Oppgave1b)
;(cons-stream
; (apply proc (map stream-car argstreams))
; (apply stream-map
;        (cons proc (map stream-cdr argstreams))))

(define (stream-map proc . s)
  (display s) (newline)
  (define stream1 (car s))
  (define stream2 (cdr s))
  (display stream1) (display stream2)(newline)(newline)
  (cond ((and (null? stream1) (null? stream2)) the-empty-stream)
        ((and (not (null? stream1)) (null? stream2)) (cons-stream
                                                      (apply proc (map stream1))
                                                      (apply stream-map (cons proc (map stream2)))))
        ((and (null? stream1) (null? (cdr stream2))) the-empty-stream)
        ((and (null? stream1) (not (null? (cdr stream2)))) (cons-stream
                                                            (apply proc (map stream-car s))
                                                            (apply stream-map (cons proc (map stream-cdr s)))))
        (else (cons-stream
               (apply proc (map stream-car s))
               (apply stream-map
                      (cons proc (map stream-cdr s)))))))

(show-stream (stream-map + one-two))
#| Oppgave1c)
Vi kan tenke oss at det at siden strømmen viser #<promise> frem til det blir kalt,
gjør at det er vanskelig for en slik metode å sammeligne alle verdier. 
|#

#| Oppgave1d)
Det som vises for første kommando er:
-> 0
Dette er fordi, de resterende verdien ikke er kalt og vi har (0 . #<promise>)
? (stream-ref x 5)
-> 1
-> 2
-> 3
-> 4
-> 5
-> 5
Her har vi hentet frem 5 verdier ved hjelp av ref, og har fra 6-10 som promise. 
? (stream-ref x 7)
-> 6
-> 7
-> 7
Her får vi bare ut 6 og 7, dette er fordi promise for de andre allered er oppfylt og ref henter dem ikke ut.
|#

;; Oppgave2a)
#|(define (bigrammer)
  (define make-lm
    (list '*text*))

  (define (lm-lookup-bigram lm string1 string2)
    (let ((subtable
           (assoc string1 (cdr lm))))
      (if subtable
          (let ((record
                 (assoc string2 (cdr subtable))))
            (if record
                (cdr record)
                false))
          false)))

  (define (lm-record-bigram! lm string1 string2)
    (let ((subtable (assoc string1 (cdr lm))))
      (if subtable
          (let ((record (assoc string2 (cdr subtable))))
            (if record
                (set-cdr! record (+ 1 (cdr record)))
                (set-cdr! subtable
                          (cons (cons string2 1)
                                (cdr lm)))))
          (set-cdr! lm
                    (cons (list (cons string1 1)
                                (cons string2 1))
                          (cdr lm))))
      'ok)
    )
  

  )|#