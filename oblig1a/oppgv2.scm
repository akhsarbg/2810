;;if variant
(define (sign x)
  (if (positive? x)
      1
      (if (zero? x) 0 -1)))

;;cond variant
(define (sign x)
  (cond ((positive? x) 1)
        ((zero? x) 0)
        (else -1)))
