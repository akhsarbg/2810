#lang r2rs

;;a)

;;add1
(define (add1 x)
  (+ x 1))

;;sub1
(define (sub1 x)
  (- x 1))

;;b)
;;itertive
(define (plus x y)
  (if (zero? y)
      x
      (plus (add1 x) (sub1 y))))

;;c)
;;recursive
(define (plus-rec x y)
  (if (zero? y)
      x
      (add1 (sub1 (plus-rec (add1 x) (sub1 y))))))

;;d)
(define (power-close-to b n)
  (define (power-iter e)
    (if (> (expt b e) n)
	e
	(power-iter (+ 1 e))))
  (power-iter 1))



;;e)
(define (fib n)
  (define (fib-iter a b count)
    (if (= count 0)
	b
	(fib-iter (+ a b) a (- count 1))))
  (fib-iter 1 0 n))
