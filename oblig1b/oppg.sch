;;Oppgave 1

;;f)
(define foo '(0 42 #t bar))
(cadr foo)
; -> 42

;;g)
(define foo '((0 42) (#t bar)))
(cdar foo)
; -> 42

;;h)
(define foo '((0) (42 #t) (bar)))
(caadr foo)
; -> 42

;;i)
;;cons-varian
(cons (cons 0 (cons 42 '())) (cons (cons #t (cons 'bar '())) '()))((0 42) (#t bar))
; -> ((0 42) (#t bar))

;;list-variant
(list (list 0 42) (list #t 'bar))
; -> ((0 42) (#t bar))


;;Oppgave 2

;;a)
(define (length2 list)
  (define (iter list len)
    (if (null? list)
        len
        (iter (cdr list) (+ 1 len))))
  (iter list 0))

;;b)
(define (fold-right-iter proc init items)
  (define (iter in out)
    (if (null? in)
        out
        (iter (cdr in) (proc (car in) out))))
  (iter items init))

(define (fold-right2 proc out in)
  (if (null? in)
      out
      (fold-right2 proc (proc (car in) out) (cdr in))))

;;c)
(define (all? proc in)
  (if (null? in)
      #t
      (if (proc (car in))
          (all? proc (cdr in))
          #f)))


(all?  (lambda (x) (< x 10))  '(1 2 3 4 5))  ;;; -> #t
(all?  (lambda (x) (< x 10))  '(1 2 3 4 50)) ;;; -> #f

;;d)
(define (nth i in)
  (if (zero? i)
      (car in)
      (nth (- i 1) (cdr in))))

;;e)
(define (where element list)
  (define (iter nth list)
    (if (null? list)
        #f
        (if (equal? element (car list))
            nth
            (iter (+ 1 nth) (cdr list)))))

  (iter 0 list))

;;f)
(define (map2 proc list1 list2)
  (if (or (null? list1) (null? list2))
      '()
      (cons (+ (car list1) (car list2))
            (map2 proc (cdr list1) (cdr list2)))))

;;g)
(map2 (lambda (x y) (/ (+ x y) 2)) '(1 2 3 4) '(3 4 5))

;;h)
(define (both? proc)
  (lambda (x y) (and (proc x)
		     (proc y))))

;;i)
(define (self? proc)
  (lambda (x) (proc x x)))
