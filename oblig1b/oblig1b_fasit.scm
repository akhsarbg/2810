;;;;
;;;; Løsningsforslag til innlevering 1b, INF2810, våren 2017
;;;; (erikve og oe)
;;;; 


;;;
;;; Oppgave 1
;;;

;; 1 a)
;; 
;; (cons 47 11) --> (47 . 11)
;;
;;
;;   +-------+-------+
;;   |       |       |
;;   |   o   |   o---+------>  11
;;   |   |   |       |
;;   +---+---+-------+
;;       |
;;       |
;;       |
;;       V
;;
;;       47


;; 1 b)
;; 
;; (cons 47 '()) --> (47)
;;
;;
;;   +-------+-------+
;;   |       |    /  |
;;   |   o   |   /   |
;;   |   |   |  /    |
;;   +---+---+-------+
;;       |
;;       |
;;       |
;;       V
;;
;;       47


;; 1 c) 
;;
;; (list 47 11) --> (47 11)
;;
;;
;;   +-------+-------+        +-------+-------+ 
;;   |       |       |        |       |    /  |
;;   |   o   |   o---+------> |   o   |   /   |
;;   |   |   |       |        |   |   |  /    |
;;   +---+---+-------+        +---+---+-------+ 
;;       |                        |
;;       |                        |
;;       |                        |
;;       V                        V 
;;
;;       47                       11    


;; 1 d)
;;
;; '(47 (11 12)) --> (47 (11 12))
;;
;;
;;   +-------+-------+        +-------+-------+ 
;;   |       |       |        |       |    /  | 
;;   |   o   |   o---+------> |   o   |   /   |  
;;   |   |   |       |        |   |   |  /    | 
;;   +---+---+-------+        +---+---+-------+ 
;;       |                        |
;;       |                        |
;;       |                        |
;;       V                        V
;; 
;;       47                   +-------+-------+        +-------+-------+
;;                            |       |       |        |       |    /  |
;;                            |   o   |   o---+------> |   o   |   /   |
;;                            |   |   |       |        |   |   |  /    |
;;                            +---+---+-------+        +---+---+-------+
;;                                |                        |
;;                                |                        |
;;                                |                        |
;;                                V                        V
;;
;;                                11                       12
			    
			    
;; 1 e)			    
;;			    
;; (define foo '(1 2 3))    
;; (cons foo foo) --> ((1 2 3) 1 2 3)
;;			    
;;			    
;;         +-------+-------+
;;         |       |       |
;;         |   o   |   o   |
;;         |   |   |  /	   |
;;         +---+---+-/-+---+
;;             |    /  	    
;;             |   /   	    
;;             |  /    	    
;;             V L     	    
;;		 	    
;;         +-------+-------+        +-------+-------+        +-------+-------+
;;         |       |       |        |       |       |        |       |    /  |
;; foo---> |   o   |   o---+------> |   o   |   o---+------> |   o   |   /   |
;;         |   |   |       |        |   |   |       |        |   |   |  /    |
;;         +---+---+-------+        +---+---+-------+        +---+---+-------+
;;             |                        |                        |
;;             |                        |                        |
;;             |                        |                        |
;;             V                        V                        V
;;			    
;;             2                        2                        3
			    
			    
;; 1 f)			    
;;			    
;; (car (cdr '(0 42 #t bar)))
;; 			    
;; nb: det finnes også en "forkortelse" for dette: 
;;			    
;; cadr
			    
			    
;; 1 g)			    
;;
;; (car (cdr (car '((0 42) (#t bar)))))
;;
;; Kan også skrives med cadar.


;; 1 h)
;;
;; (car (car (cdr '((0) (42 #t) (bar)))))
;;
;; Kan også skrives med caadr.


;; 1 i)
;;
;; (cons (cons 0 (cons 42 '()))
;;       (cons (cons #t (cons 'bar '())) '()))
;;
;;
;; (list (list 0 42)
;;       (list #t 'bar))


;; 2 a)

(define (length2 items)  
  (define (iter rest n)  
    (if (null? rest)  
	n  
        (iter (cdr rest) (+ n 1))))      
  (iter items 0))  
 
 
;; 2 b)

(define (reduce-reverse proc init items) 
  (if (null? items) 
      init 
      (reduce-reverse proc 
                      (proc (car items) init)
                      (cdr items))))

;; Alternativ versjon med intern hjelpeprosedyre:

(define (reduce-reverse2 proc init items) 
  (define (iter in out)
    (if (null? in) 
        out
        (iter (cdr in)
              (proc (car in) out))))
  (iter items init))

;; Begge definisjonene over benytter halerekursjon: Vi ser at det ikke
;; er noe mer som gjenstår å gjøre i prosedyrekroppen etter det
;; rekursive kallet.  Dette resulterer i en iterativ prosess (med en
;; minnebruk som er konstant og en tidsbruk som er lineær i lengden på
;; input lista).
 
  
;; 2 c)

(define (all? pred seq)
  (or (null? seq)
      (and (pred (car seq))
           (all? pred (cdr seq)))))

;; Kall med anonym prosedyre som sjekker om elementene er <10:
;;
;; (all? (lambda (x) (< x 10)) '(1 2 3 4 5))
 
   
;; 2 d)

(define (nth n items)
  (if (zero? n)
      (car items)
      (nth (- n 1) (cdr items))))
 
    
;; 2 e)

(define (where x seq)  
  (define (w-iter rest pos)  
    (cond ((null? rest) #f)  
	  ((= x (car rest)) pos)  
	  (else (w-iter (cdr rest) (+ pos 1)))))  
  (w-iter seq 0))  
  
     
;; 2 f)

(define (map2 proc list1 list2)
  (if (or (null? list1) 
	  (null? list2))
      '()
      (cons (proc (car list1)
                  (car list2))
            (map2 proc 
		  (cdr list1) 
		  (cdr list2)))))
 
      
;; 2 g)

;; (map2 (lambda (x y) 
;;          (/ (+ x y) 2)) 
;;       '(1 2 3 4) '(3 4 5))
;;
;; --> (2 3 4)
 
       
;; 2 h)

(define (both? pred)
  (lambda (x y)
    (and (pred x)
	 (pred y))))
 
        
;; 2 i)

(define (self proc)
  (lambda (x)
    (proc x x)))
 
         
