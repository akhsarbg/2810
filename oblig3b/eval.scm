(load "evaluator.scm")
(set! the-global-environment (setup-environment))

;;(read-eval-print-loop)
(newline)(newline)(display "Oppgave 1a")(newline)
(mc-eval '(define (foo cond else)
            (cond ((= cond 2) 0)
                  (else (else cond)))) the-global-environment)
(mc-eval '(define cond 3) the-global-environment)
(mc-eval '(define (else x) (/ x 2)) the-global-environment)
(mc-eval '(define (square x) (* x x)) the-global-environment)

;;resultatet av kallet under er 0, fordi testen (= cond 2) står
(newline)(newline)(newline)(newline)(newline)(newline)(newline)
(display '(foo 2 square)) (display " -> ")
(mc-eval '(foo 2 square) the-global-environment)

;;resultatet av kallet under er 16, fordi testen (= cond 2) feiler
;;derfor blir (else cond) evaluert, men else har blit definert som square
;;derfor ender vi med (square 4) som er 16
(display '(foo 4 square)) (display " -> ")
(mc-eval '(foo 4 square) the-global-environment)

;;resultated av kallet under er 2, (= cond 2) -> (= 3 2) feiler
;;derfor blir else grenen evaluert som består av (else 4), men
;; 'else har blitt definert til funsjon som deler argumentet på 2
;; så (else 4) -> (/ 4 2) -> 2
(display '(cond ((= cond 2) 0)
              (else (else 4)))) (display " -> ")
(mc-eval '(cond ((= cond 2) 0)
              (else (else 4))) the-global-environment)

;;Hvorfor akuratt dette sjer?
;;

;;2a
(newline)(newline)(display "Oppgave 2a")(newline)
(display '(1+ 2)) (display " -> ")
(mc-eval '(1+ 2) the-global-environment)
(display '(1- 2)) (display " -> ")
(mc-eval '(1- 2) the-global-environment)


;;2b
(newline)(newline)(display "Oppgave 2b")(newline)
(display '(square2 2)) (display " -> ")
(mc-eval '(square2 2) the-global-environment)
(install-primitive! 'square2 (lambda (x) (* x x)))
(display '(install-primitive! 'square2 (lambda (x) (* x x))))
(newline)(display '(square2 2)) (display " -> ")
(mc-eval '(square2 2) the-global-environment)

;;3a
;;test
(newline)(newline)(display "Oppgave 3a")(newline)
(display '(and 1 3)) (display " -> ")
(mc-eval '(and 1 3) the-global-environment)
(display '(or 0 1)) (display " -> ")
(mc-eval '(or 0 1) the-global-environment)


;;3b
(newline)(newline)(display "Oppgave 3b")(newline)
(define if-orig '(if #f #t #f))

(define if-elsif-else '(if #f
                           then 1
                           elsif #f
                           then 2
                           elsif #f
                           then 3
                           else 4))
(display if-orig) (display " -> ")
(mc-eval if-orig the-global-environment)
(display if-elsif-else) (display " -> ")
(mc-eval if-elsif-else the-global-environment)

;;3c
(newline)(newline)(display "Oppgave 3c")(newline)
(define let '(let ((a 1)
                   (b 2)
                   (c 3))
               (+ a (+ b c))))
(display let) (display " -> ")
(mc-eval let the-global-environment)

;;3d
(newline)(newline)(display "Oppgave 3d")(newline)
(define let2 '(let v1 = 1 and
                v2 = 2 and
                v3 = 3 in
                (+ v1 (+ v2 v3))))
(display let2) (display " -> ")
(mc-eval let2 the-global-environment)

;;3e
(newline)(newline)(display "Oppgave 3e")(newline)
(define while '(while (not (eq? i 0))
                      (begin
                        (display i) (newline)
                        (set! i (- i 1)))))

(display '(define i 3)) (display " -> ")
(mc-eval '(define i 3) the-global-environment)
(mc-eval while the-global-environment)


